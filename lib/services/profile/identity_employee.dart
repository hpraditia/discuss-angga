import 'package:dio/dio.dart';
import 'package:moyee/models/employee/identity.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/values.dart';

class IdentityEmployeeService {
  Future<EmployeeIdentityModel> getData(UserModel data) async {
    try {
      print(data.employeeId);
      final res = await Dio().get(
        '$baseUrl/index.php/myradiant/profil/general',
        queryParameters: {
          'id': data.employeeId,
        },
      );

      if (res.statusCode == 200 || res.statusCode == 201) {
        if (!res.data['success']) {
          throw res.data['msg'];
        }

        print(res.data['data']);
        EmployeeIdentityModel employee =
            EmployeeIdentityModel.fromJson(res.data['data']);

        return employee;
      } else {
        throw res.data['msg'];
      }
    } catch (e) {
      rethrow;
    }
  }
}
