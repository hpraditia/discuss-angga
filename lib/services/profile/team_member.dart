import 'package:dio/dio.dart';
import 'package:moyee/models/employee/team_member.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/values.dart';

class TeamMemberService {
  Future<List<TeamMemberModel>> getMember(UserModel data) async {
    try {
      final res = await Dio().get(
        '$baseUrl/index.php/attendance/app/attendance_bawahan',
        queryParameters: {
          'id': data.employeeId,
          'nik': data.username,
          'limit': 5,
        },
      );

      if (res.statusCode == 200 || res.statusCode == 201) {
        if (!res.data['success']) {
          throw res.data['msg'];
        }

        List<TeamMemberModel> members =
            List<TeamMemberModel>.from(res.data['data'].map(
          (member) => TeamMemberModel.fromJson(member),
        )).toList();
        return members;
      } else {
        throw res.data['msg'];
      }
    } catch (e) {
      rethrow;
    }
  }
}
