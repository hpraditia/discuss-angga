import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:moyee/models/auth/login_form.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/values.dart';

class AuthService {
  Future<UserModel> login(LoginModel data) async {
    try {
      final res = await Dio().post(
        '$baseUrl/index.php/login',
        data: data,
        // options: Options(
        //   contentType: Headers.formUrlEncodedContentType,
        // ),
      );

      if (res.statusCode == 200 || res.statusCode == 201) {
        if (!res.data['success']) {
          throw res.data['msg'];
        }

        UserModel user = UserModel.fromJson(res.data['data']);
        user = user.copyWith(password: data.password);

        await storeCredentialToLocal(user);

        return user;
      } else {
        throw res.data['msg'];
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> storeCredentialToLocal(UserModel user) async {
    try {
      const storage = FlutterSecureStorage();

      // await storage.write(key: 'token', value: user.token);
      await storage.write(key: 'username', value: user.username);
      await storage.write(key: 'password', value: user.password);
    } catch (e) {
      rethrow;
    }
  }

  Future<String> getToken() async {
    String token = '';

    const storage = FlutterSecureStorage();
    String? value = await storage.read(key: 'token');

    if (value != null) {
      token = value;
    }

    return token;
  }

  Future<LoginModel> getCredentialFromLocal() async {
    try {
      // print('get user from local');
      const storage = FlutterSecureStorage();
      Map<String, String> values = await storage.readAll();

      // print('values  => $values');

      // if (values['token'] != null) {
      if (values['username'] != null && values['password'] != null) {
        final LoginModel data = LoginModel(
          username: values['username'],
          password: values['password'],
        );

        // print('get user from local: ${data.toJson()}');

        return data;
      } else {
        throw 'unauthenticated';
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> clearLocalStorage() async {
    try {
      // print('clear');
      const storage = FlutterSecureStorage();
      await storage.deleteAll();
    } catch (e) {
      rethrow;
    }
  }
}
