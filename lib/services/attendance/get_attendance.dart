import 'package:dio/dio.dart';
import 'package:moyee/models/attendance/attendance.dart';
import 'package:moyee/models/attendance/parameter.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/values.dart';

class GetAttendanceService {
  Future<List<AttendanceModel>> getData(UserModel data) async {
    try {
      final res = await Dio().get(
        '$baseUrl/index.php/attendance/App',
        queryParameters: {
          'id': data.employeeId,
          'nik': data.username,
        },
      );
      if (res.statusCode == 200 || res.statusCode == 201) {
        if (!res.data['success']) {
          throw res.data['msg'];
        }

        List<AttendanceModel> attendances =
            List<AttendanceModel>.from(res.data['data'].map(
          (attendance) => AttendanceModel.fromJson(attendance),
        )).toList();

        return attendances;
      } else {
        throw res.data['msg'];
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<List<AttendanceModel>> getDashboardData(
      ParameterAttendanceFormModel data) async {
    try {
      final res = await Dio().get(
        '$baseUrl/index.php/attendance/App',
        queryParameters: data.toJson(),
      );
      if (res.statusCode == 200 || res.statusCode == 201) {
        if (!res.data['success']) {
          throw res.data['msg'];
        }

        List<AttendanceModel> attendances =
            List<AttendanceModel>.from(res.data['data'].map(
          (attendance) => AttendanceModel.fromJson(attendance),
        )).toList();

        return attendances;
      } else {
        throw res.data['msg'];
      }
    } catch (e) {
      rethrow;
    }
  }
}
