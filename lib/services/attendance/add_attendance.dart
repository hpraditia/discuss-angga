import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:moyee/models/attendance/add_form.dart';
import 'package:moyee/shared/values.dart';

class AddAttendanceService {
  Future<dynamic> save(AddAttendanceFormModel data) async {
    try {
      final res = await Dio().post(
        '$baseUrl/index.php/attendance/app/add',
        data: jsonEncode(data.toJson()),
      );
      // print(res);

      if (res.statusCode == 200 || res.statusCode == 201) {
        if (!res.data['success']) {
          throw res.data['msg'];
        }

        return res.data;
      } else {
        throw res.data['msg'];
      }
    } catch (e) {
      print(e);
      rethrow;
    }
  }
}
