import 'package:dio/dio.dart';
import 'package:moyee/models/attendance/today.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/values.dart';

class TodayAttendanceService {
  Future<TodayAttendanceModel> getData(UserModel data) async {
    try {
      final res = await Dio().get(
        '$baseUrl/index.php/attendance/app/info_attendance',
        queryParameters: {
          'id': data.employeeId,
        },
      );

      if (res.statusCode == 200 || res.statusCode == 201) {
        if (!res.data['success']) {
          throw res.data['msg'];
        }

        TodayAttendanceModel attendance =
            TodayAttendanceModel.fromJson(res.data['data']);

        return attendance;
      } else {
        throw res.data['msg'];
      }
    } catch (e) {
      rethrow;
    }
  }
}
