import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moyee/blocs/attendance/attendance_bloc.dart';
import 'package:moyee/blocs/auth/auth_bloc.dart';
import 'package:moyee/blocs/employee/employee_bloc.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/pages/attendance/add.dart';
import 'package:moyee/ui/pages/auth/login.dart';
import 'package:moyee/ui/pages/auth/pin.dart';
import 'package:moyee/ui/pages/home/home.dart';
import 'package:moyee/ui/pages/splash/splash.dart';
import 'package:moyee/ui/pages/user/change_password.dart';
import 'package:moyee/ui/pages/user/edit_profile.dart';
import 'package:moyee/ui/pages/user/profile.dart';
import 'package:moyee/ui/pages/user/success.dart';

import 'package:moyee/ui/pages/attendance/attendance.dart';
import 'ui/pages/user/team_member.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthBloc()
            ..add(
              AuthGetCurrentUser(),
            ),
        ),
        BlocProvider(
          create: (context) => EmployeeBloc(),
        ),
        BlocProvider(
          create: (context) => AttendanceBloc(),
        ),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            scaffoldBackgroundColor: lightBackgroundColor,
            appBarTheme: AppBarTheme(
              backgroundColor: lightBackgroundColor,
              elevation: 0,
              centerTitle: true,
              iconTheme: IconThemeData(color: blackColor),
              titleTextStyle: blackTextStyle.copyWith(
                fontSize: 20,
                fontWeight: bold,
              ),
            ),
            datePickerTheme: DatePickerThemeData(
              rangePickerHeaderBackgroundColor: primaryColor,
              rangePickerHeaderForegroundColor: blackColor,
              rangePickerHeaderHeadlineStyle: whiteTextStyle,
              rangePickerSurfaceTintColor: primaryColor,
              headerBackgroundColor: primaryColor,
            ),
          ),
          routes: {
            '/': (context) => const SplashPage(),
            '/login': (context) => const LoginPage(),
            '/home': (context) => const HomePage(),
            '/profile': (context) => const ProfilePage(),
            '/team-member': (context) => const TeamMemberPage(),
            '/attendance-apps': (context) => const AppsAttendance(),
            '/pin': (context) => const PinPage(),
            '/edit-profile': (context) => const EditProfilePage(),
            '/add-attendance': (context) => const AddAttendancePage(),
            '/change-password': (context) => const ChangePasswordPage(),
            '/success-edit-profile': (context) => const SuccessPage(),
          }),
    );
  }
}
