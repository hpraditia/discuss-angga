class UserModel {
  final int? id;
  final String? name;
  final String username;
  final String? profilePicture;
  final String? token;
  final String? password;
  final String employeeId;

  UserModel({
    this.id,
    this.name,
    required this.username,
    this.profilePicture,
    this.token,
    this.password,
    required this.employeeId,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: int.parse(json["id"]),
        name: json["user_name"],
        username: json["user_login"],
        profilePicture: json["profile_picture"],
        token: json["token"],
        employeeId: json["Employee_ID"],
      );

  UserModel copyWith({
    String? name,
    String? username,
    String? password,
    String? profilePicture,
  }) =>
      UserModel(
        id: id,
        username: username ?? this.username,
        name: name ?? this.name,
        password: password ?? this.password,
        profilePicture: profilePicture ?? this.profilePicture,
        token: token,
        employeeId: employeeId,
      );
}
