class ParameterAttendanceFormModel {
  final String? employeeId;
  final String? employeeCode;
  final String? keyword;
  final String? startDate;
  final String? endDate;
  final String? limit;
  final String? userProject;
  final String? orderBy;

  ParameterAttendanceFormModel({
    this.employeeId,
    this.employeeCode,
    this.keyword,
    this.startDate,
    this.endDate,
    this.limit,
    this.userProject,
    this.orderBy,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': employeeId,
      'code': employeeCode,
      'search': keyword,
      'date_start': startDate,
      'date_end': endDate,
      'limit': limit,
      'user_project': userProject,
      'order_by': orderBy,
    };
  }
}
