class AddAttendanceFormModel {
  final String longlat;
  final String address;
  final String time;
  final String date;
  final String type;
  final String description;
  final String status;
  final String employeeId;
  final String employeeCode;

  AddAttendanceFormModel({
    required this.longlat,
    required this.address,
    required this.time,
    required this.date,
    required this.type,
    required this.description,
    required this.status,
    required this.employeeId,
    required this.employeeCode,
  });

  Map<String, dynamic> toJson() {
    return {
      'coordinate': longlat,
      'address': address,
      'full_date': time,
      'date_attendance': date,
      'type': type,
      'notes': description,
      'id': employeeId,
      'code': employeeCode,
      'clock_type': status,
    };
  }
}
