class AttendanceModel {
  final String id;
  final String date;
  final String type;
  final String employeeId;
  final String employeeCode;
  final String coordinate;
  final String clockType;
  final String notes;
  final String address;
  final String formatTime;
  final String shortTime;
  final String employeeName;

  AttendanceModel({
    required this.id,
    required this.date,
    required this.type,
    required this.employeeId,
    required this.employeeCode,
    required this.coordinate,
    required this.clockType,
    required this.notes,
    required this.address,
    required this.formatTime,
    required this.shortTime,
    required this.employeeName,
  });

  factory AttendanceModel.fromJson(Map<String, dynamic> json) =>
      AttendanceModel(
        id: json['id'] ?? "-",
        date: json['date_text'] ?? "-",
        type: json['attendance_type'] ?? "-",
        employeeId: json['employee_Id'] ?? "-",
        employeeCode: json['employee_code'] ?? "-",
        coordinate: json['coordinate'] ?? "-",
        clockType: json['clock_type'] ?? "-",
        notes: json['notes'] ?? "-",
        address: json['attendance_address'] ?? "-",
        formatTime: json['format_time'] ?? "-",
        shortTime: json['short_time'] ?? "-",
        employeeName: json['employee_name'] ?? "-",
      );

  AttendanceModel getData() => AttendanceModel(
        id: id,
        date: date,
        type: type,
        employeeId: employeeId,
        employeeCode: employeeCode,
        coordinate: coordinate,
        clockType: clockType,
        notes: notes,
        address: address,
        formatTime: formatTime,
        shortTime: shortTime,
        employeeName: employeeName,
      );
}
