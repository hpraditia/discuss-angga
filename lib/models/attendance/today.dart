class TodayAttendanceModel {
  final String attendanceDate;
  final String attendanceText;
  final String attendanceTime;
  final String attendanceStatus;
  final String attendanceType;
  final String attendancePrevious;
  final String attendanceLabel;
  final String attendanceFullDateTime;
  final Map<String, dynamic> attendanceLastData;

  TodayAttendanceModel({
    required this.attendanceDate,
    required this.attendanceText,
    required this.attendanceTime,
    required this.attendanceStatus,
    required this.attendanceType,
    required this.attendancePrevious,
    required this.attendanceLabel,
    required this.attendanceFullDateTime,
    required this.attendanceLastData,
  });

  factory TodayAttendanceModel.fromJson(Map<String, dynamic> json) =>
      TodayAttendanceModel(
        attendanceDate: json["attendance_date"],
        attendanceText: json["attendance_date_text"],
        attendanceTime: json["time"],
        attendanceStatus: json["type"],
        attendanceType: json["attendance_type"],
        attendanceLabel: 'Kamis, 22 Desember 2022',
        attendanceLastData: json["last"],
        attendanceFullDateTime: json["full_date"],
        attendancePrevious: json["previous_date"],
      );

  TodayAttendanceModel getData() => TodayAttendanceModel(
      attendanceDate: attendanceDate,
      attendanceText: attendanceText,
      attendanceTime: attendanceTime,
      attendanceStatus: attendanceStatus,
      attendanceType: attendanceType,
      attendancePrevious: attendancePrevious,
      attendanceLabel: attendanceLabel,
      attendanceFullDateTime: attendanceFullDateTime,
      attendanceLastData: attendanceLastData);
}
