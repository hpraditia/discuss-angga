class EmployeeIdentityModel {
  final String? id;
  final String? name;
  final String? email;
  final String? gender;
  final String? placeOfBirth;
  final String? dateOfBirth;
  final String? address;

  EmployeeIdentityModel({
    this.id,
    this.name,
    this.email,
    this.gender,
    this.placeOfBirth,
    this.dateOfBirth,
    this.address,
  });

  factory EmployeeIdentityModel.fromJson(Map<String, dynamic> json) =>
      EmployeeIdentityModel(
        id: json["code"],
        name: json["name"],
        email: json["email"],
        gender: json["gender"],
        placeOfBirth: json["place_of_birth"],
        dateOfBirth: json["date_of_birth"],
        address: json["address"],
      );

  EmployeeIdentityModel copyWith({
    String? name,
    String? email,
    String? placeOfBirth,
    String? address,
  }) =>
      EmployeeIdentityModel(
        id: id,
        name: name ?? this.name,
        email: email ?? this.email,
        gender: gender,
        placeOfBirth: placeOfBirth ?? this.placeOfBirth,
        dateOfBirth: dateOfBirth,
        address: address ?? this.address,
      );
}
