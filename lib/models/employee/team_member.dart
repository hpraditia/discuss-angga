class TeamMemberModel {
  final String id;
  final String name;
  final String profilePicture;
  final String code;
  final String email;
  final String position;

  TeamMemberModel({
    required this.id,
    required this.name,
    required this.profilePicture,
    required this.code,
    required this.email,
    required this.position,
  });

  factory TeamMemberModel.fromJson(Map<String, dynamic> json) =>
      TeamMemberModel(
        id: json["id"],
        name: json["name"],
        profilePicture: json["employee_image"],
        code: json["nik"],
        position: json["jabatan"],
        email: json["email"] ?? '-',
      );
}
