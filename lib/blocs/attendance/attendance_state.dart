part of 'attendance_bloc.dart';

abstract class AttendanceState extends Equatable {
  const AttendanceState();

  @override
  List<Object> get props => [];
}

class AttendanceInitial extends AttendanceState {}

class AttendanceLoading extends AttendanceState {}

class AttendanceFailed extends AttendanceState {
  final String e;
  const AttendanceFailed(this.e);

  @override
  List<Object> get props => [e];
}

class AttendanceTodaySuccess extends AttendanceState {
  final TodayAttendanceModel data;
  const AttendanceTodaySuccess(this.data);

  @override
  List<Object> get props => [data];
}

class AddAttendanceSuccess extends AttendanceState {}

class GetAttendanceSuccess extends AttendanceState {
  final List<AttendanceModel> data;
  const GetAttendanceSuccess(this.data);

  @override
  List<Object> get props => [data];
}
