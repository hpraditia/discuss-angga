import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:moyee/models/attendance/add_form.dart';
import 'package:moyee/models/attendance/attendance.dart';
import 'package:moyee/models/attendance/parameter.dart';
import 'package:moyee/models/attendance/today.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/services/attendance/add_attendance.dart';
import 'package:moyee/services/attendance/get_attendance.dart';
import 'package:moyee/services/attendance/today_attendance.dart';

part 'attendance_event.dart';
part 'attendance_state.dart';

class AttendanceBloc extends Bloc<AttendanceEvent, AttendanceState> {
  AttendanceBloc() : super(AttendanceInitial()) {
    on<AttendanceEvent>((event, emit) async {
      if (event is AttendanceGetData) {
        try {
          emit(AttendanceLoading());
          // print('getting data attendances');
          final data = await GetAttendanceService().getData(event.data);
          // print(data);
          emit(GetAttendanceSuccess(data));
        } catch (e) {
          emit(AttendanceFailed(e.toString()));
        }
      }

      if (event is AttendanceGetDashboardData) {
        try {
          emit(AttendanceLoading());
          print("getting data dashboards");
          final data =
              await GetAttendanceService().getDashboardData(event.data);
          emit(GetAttendanceSuccess(data));
        } catch (e) {
          emit(AttendanceFailed(e.toString()));
        }
      }

      if (event is AttendanceRefreshDashboardData) {
        try {
          emit(AttendanceLoading());
          print("getting data dashboards");
          final data =
              await GetAttendanceService().getDashboardData(event.data);

          emit(GetAttendanceSuccess(data));
        } catch (e) {
          emit(AttendanceFailed(e.toString()));
        }
      }

      if (event is AttendanceGetDataToday) {
        try {
          emit(AttendanceLoading());

          final TodayAttendanceModel data =
              await TodayAttendanceService().getData(event.data);

          emit(AttendanceTodaySuccess(data));
        } catch (e) {
          emit(AttendanceFailed(e.toString()));
        }
      }

      if (event is AddAttendance) {
        try {
          emit(AttendanceLoading());

          await AddAttendanceService().save(event.data);

          emit(AddAttendanceSuccess());
        } catch (e) {
          emit(AttendanceFailed(e.toString()));
        }
      }
    });
  }
}
