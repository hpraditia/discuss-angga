part of 'attendance_bloc.dart';

abstract class AttendanceEvent extends Equatable {
  const AttendanceEvent();

  @override
  List<Object> get props => [];
}

class AttendanceGetDataToday extends AttendanceEvent {
  final UserModel data;
  const AttendanceGetDataToday(this.data);

  @override
  List<Object> get props => [data];
}

class AttendanceGetData extends AttendanceEvent {
  final UserModel data;
  const AttendanceGetData(this.data);

  @override
  List<Object> get props => [data];
}

class AttendanceGetDashboardData extends AttendanceEvent {
  final ParameterAttendanceFormModel data;
  const AttendanceGetDashboardData(this.data);

  @override
  List<Object> get props => [data];
}

class AttendanceRefreshDashboardData extends AttendanceEvent {
  final ParameterAttendanceFormModel data;
  const AttendanceRefreshDashboardData(this.data);

  @override
  List<Object> get props => [data];
}

class AddAttendance extends AttendanceEvent {
  final AddAttendanceFormModel data;
  const AddAttendance(this.data);

  @override
  List<Object> get props => [data];
}
