import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:moyee/models/employee/identity.dart';
import 'package:moyee/models/employee/team_member.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/services/profile/identity_employee.dart';
import 'package:moyee/services/profile/team_member.dart';

part 'employee_event.dart';
part 'employee_state.dart';

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  EmployeeBloc() : super(EmployeeInitial()) {
    on<EmployeeEvent>((event, emit) async {
      if (event is GetEmployeeData) {
        try {
          emit(EmployeeLoading());

          final res = await IdentityEmployeeService().getData(event.data);

          emit(EmployeeSuccess(res));
        } catch (e) {
          emit(EmployeeFailed(e.toString()));
        }
      }

      if (event is GetEmployeeTeamMember) {
        try {
          emit(EmployeeLoading());
          print('getting data member');
          final res = await TeamMemberService().getMember(event.data);

          emit(EmployeeTeamMemberSuccess(res));
        } catch (e) {
          emit(EmployeeFailed(e.toString()));
        }
      }
    });
  }
}
