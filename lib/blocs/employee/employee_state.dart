part of 'employee_bloc.dart';

abstract class EmployeeState extends Equatable {
  const EmployeeState();

  @override
  List<Object> get props => [];
}

class EmployeeInitial extends EmployeeState {}

class EmployeeLoading extends EmployeeState {}

class EmployeeFailed extends EmployeeState {
  final String e;
  const EmployeeFailed(this.e);

  @override
  List<Object> get props => [e];
}

class EmployeeDone extends EmployeeState {}

class EmployeeSuccess extends EmployeeState {
  final EmployeeIdentityModel data;
  const EmployeeSuccess(this.data);

  @override
  List<Object> get props => [data];
}

class EmployeeTeamMemberSuccess extends EmployeeState {
  final List<TeamMemberModel> data;
  const EmployeeTeamMemberSuccess(this.data);

  @override
  List<Object> get props => [data];
}
