part of 'employee_bloc.dart';

abstract class EmployeeEvent extends Equatable {
  const EmployeeEvent();

  @override
  List<Object> get props => [];
}

class GetEmployeeData extends EmployeeEvent {
  final UserModel data;
  const GetEmployeeData(this.data);

  @override
  List<Object> get props => [data];
}

class GetEmployeeTeamMember extends EmployeeEvent {
  final UserModel data;
  const GetEmployeeTeamMember(this.data);

  @override
  List<Object> get props => [data];
}
