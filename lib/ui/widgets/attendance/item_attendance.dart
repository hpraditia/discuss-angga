import 'package:flutter/material.dart';
import 'package:moyee/shared/theme.dart';

class AttendanceItem extends StatelessWidget {
  final String date;
  final String time;
  final String id;
  final String type;
  final String employeeId;
  final String employeeCode;
  final String coordinate;
  final String clockType;
  final String notes;
  final String address;
  final String formatTime;
  final String employeeName;
  // final String

  const AttendanceItem({
    Key? key,
    required this.id,
    required this.date,
    required this.type,
    required this.employeeId,
    required this.employeeCode,
    required this.coordinate,
    required this.clockType,
    required this.notes,
    required this.address,
    required this.formatTime,
    required this.employeeName,
    required this.time,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 120,
        margin: const EdgeInsets.only(bottom: 17),
        decoration: BoxDecoration(
          color: clockType == 'Clock In' ? const Color(0xFF007aff) : blackColor,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(children: [
          Container(
            width: MediaQuery.of(context).size.width / 2.5,
            height: 120,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/art_attendance_1.png"),
                fit: BoxFit.fill,
                opacity: 0.19,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text('${clockType} Time',
                          style: whiteTextStyle.copyWith(
                            fontSize: 14,
                          ))
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    time,
                    style: whiteTextStyle.copyWith(
                      fontWeight: bold,
                      fontSize: 30,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Asdf",
                    style: whiteTextStyle.copyWith(
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(type,
                        style: whiteTextStyle.copyWith(
                          fontWeight: semiBold,
                          fontSize: 16,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      address,
                      style: whiteTextStyle.copyWith(
                          fontSize: 14, overflow: TextOverflow.clip),
                      softWrap: false,
                      textWidthBasis: TextWidthBasis.parent,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                    ),
                  ]),
            ),
          )
        ]));
  }
}

class AttendanceItemLoading extends StatelessWidget {
  const AttendanceItemLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90,
      height: 120,
      margin: const EdgeInsets.only(right: 17),
      decoration: BoxDecoration(
        color: whiteColor,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 45,
            height: 45,
            margin: const EdgeInsets.only(bottom: 13),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: greyColor,
            ),
          ),
          Container(
            width: 60,
            height: 12,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(22),
              color: greyColor,
            ),
          ),
        ],
      ),
    );
  }
}
