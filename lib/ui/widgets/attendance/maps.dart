import 'package:flutter/material.dart';
import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:iconify_flutter/icons/eva.dart';
import 'package:iconify_flutter/icons/heroicons.dart';

import 'package:latlong2/latlong.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:moyee/shared/theme.dart';

class CustomMaps extends StatelessWidget {
  final double longitude;
  final double latitude;
  const CustomMaps({
    Key? key,
    required this.longitude,
    required this.latitude,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        center: LatLng(latitude, longitude),
        zoom: 15.0,
        enableScrollWheel: false,
        interactiveFlags: InteractiveFlag.pinchZoom,
      ),
      layers: [
        TileLayerOptions(
          urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
          subdomains: ['a', 'b', 'c'],
          userAgentPackageName: 'dev.fleaflet.flutter_map.example',
        ),
        // TileLayerOptions(
        //   urlTemplate:
        //       "https://api.mapbox.com/styles/v1/hanafiq/clc0abj7b001v14q75l1lf1y3/wmts?access_token={access_token}",
        //   additionalOptions: {
        //     "access_token":
        //         "pk.eyJ1IjoiaGFuYWZpcSIsImEiOiJjangwM2N6ZGkwZ2YzNDlwb2VueDFzYjdjIn0.gJa2aKbVqmJ-nAFn8lc1Xg",
        //   },
        //   userAgentPackageName: 'com.example.app',
        // ),
        MarkerLayerOptions(
          markers: [
            Marker(
              width: 30,
              height: 30,
              point: LatLng(latitude, longitude),
              builder: (ctx) => Iconify(
                Eva.pin_fill,
                color: primaryColor,
                //style: HeroIconStyle.solid,
              ),
            ),
          ],
        )
      ],
    );
  }
}

class CustomMapsHistory extends StatelessWidget {
  final double centerLongitude;
  final double centerLatitude;
  const CustomMapsHistory({
    Key? key,
    required this.centerLongitude,
    required this.centerLatitude,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        center: LatLng(centerLatitude, centerLongitude),
        zoom: 14.0,
        enableScrollWheel: true,
        interactiveFlags: InteractiveFlag.all,
      ),
      nonRotatedChildren: const [],
      layers: [
        TileLayerOptions(
          urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
          subdomains: ['a', 'b', 'c'],
          userAgentPackageName: 'dev.fleaflet.flutter_map.example',
        ),
        MarkerLayerOptions(
          markers: [
            Marker(
              width: 30,
              height: 30,
              point: LatLng(centerLatitude, centerLongitude),
              builder: (ctx) => Iconify(
                Eva.pin_fill,
                color: primaryColor,
                //style: HeroIconStyle.solid,
              ),
            ),
          ],
        )
      ],
    );
  }
}
