import 'package:flutter/material.dart';
import 'package:moyee/shared/theme.dart';

class PinButton extends StatelessWidget {
  final String text;
  final VoidCallback? onTap;
  const PinButton({Key? key, required this.text, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          width: 60,
          height: 60,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: numberBackgroundColor,
          ),
          child: Center(
            child: Text(text,
                style: whiteTextStyle.copyWith(
                    fontSize: 22, fontWeight: semiBold)),
          )),
    );
  }
}
