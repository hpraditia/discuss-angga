import 'package:flutter/material.dart';
import 'package:moyee/shared/theme.dart';

class RoundedCard extends StatelessWidget {
  final Color color;
  final double width;
  final double? height;
  final Widget? cardChild;
  const RoundedCard({
    Key? key,
    required this.color,
    this.cardChild,
    required this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        width: width,
        padding: const EdgeInsets.all(22),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(20),
        ),
        child: cardChild != null ? cardChild : null);
  }
}
