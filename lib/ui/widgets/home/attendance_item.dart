import 'package:flutter/material.dart';
import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:iconify_flutter/icons/eva.dart';
import 'package:iconify_flutter/icons/heroicons.dart';

import 'package:moyee/shared/theme.dart';

class AttendanceItem extends StatelessWidget {
  final String category;
  final String title;
  final String time;
  final String value;

  const AttendanceItem(
      {Key? key,
      required this.category,
      required this.title,
      required this.time,
      required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 18),
      child: Row(
        children: [
          buildIcon(category),
          const SizedBox(width: 16),
          Expanded(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                title,
                style: blackTextStyle.copyWith(
                  fontSize: 16,
                  fontWeight: medium,
                ),
              ),
              const SizedBox(height: 2),
              Text(
                time,
                style: greyTextStyle.copyWith(
                  fontSize: 12,
                ),
              ),
            ]),
          ),
          Text(category == "History" ? "Log" : category,
              style: blackTextStyle.copyWith(
                fontSize: 16,
                fontWeight: medium,
              ))
        ],
      ),
    );
  }

  Widget buildIcon(String category) {
    String? icon;
    Color? color;
    Color? iconColor;
    switch (category.toLowerCase()) {
      case 'cuti':
        icon = Heroicons.document_text_20_solid;
        color = const Color(0xFFffefdf);
        iconColor = const Color(0xFFf77000);
        break;
      case 'dinas':
        icon = Heroicons.map_20_solid;
        color = const Color(0xFFf5e7fa);
        iconColor = const Color(0xFFa12fbd);
        break;
      case 'wfo':
        icon = Heroicons.building_office_20_solid;
        color = const Color(0xFFe7f5fc);
        iconColor = const Color(0xFF3197dd);
        break;
      case 'wfh':
        icon = Heroicons.wifi_20_solid;
        color = const Color(0xFFe5f6ef);
        iconColor = const Color(0xFF2fa368);
        break;
      case 'history':
        icon = Eva.pin_outline;
        color = const Color(0xFFe5f6ef);
        iconColor = const Color(0xFF005f73);
        break;
      default:
    }
    return Container(
      width: 40,
      height: 40,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: color ?? blueColor,
        borderRadius: BorderRadius.circular(15),
      ),
      child:
          Iconify(icon ?? Eva.calendar_outline, color: iconColor ?? whiteColor),
    );
  }
}
