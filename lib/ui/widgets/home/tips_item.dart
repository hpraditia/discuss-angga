import 'package:flutter/material.dart';
import 'package:moyee/shared/theme.dart';
import 'package:url_launcher/url_launcher.dart';

class TipsItem extends StatelessWidget {
  final String imgUrl;
  final String title;
  final String url;

  const TipsItem({
    Key? key,
    required this.imgUrl,
    required this.title,
    required this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (await canLaunch(url)) {
          launch(url);
        }
      },
      child: Container(
          width: (MediaQuery.of(context).size.width / 2.36),
          height: 176,
          decoration: BoxDecoration(
            color: whiteColor,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius:
                    const BorderRadius.vertical(top: Radius.circular(20)),
                child: Image.asset(
                  imgUrl,
                  width: (MediaQuery.of(context).size.width / 2.36),
                  height: 110,
                  fit: BoxFit.cover,
                ),
              ),
              const SizedBox(height: 12),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Text(
                  title,
                  style: blackTextStyle.copyWith(
                    fontWeight: medium,
                    overflow: TextOverflow.ellipsis,
                  ),
                  maxLines: 2,
                ),
              )
            ],
          )),
    );
  }
}
