import 'package:flutter/material.dart';
import 'package:moyee/shared/theme.dart';

class ProfileMenuItem extends StatelessWidget {
  final String iconUrl;
  final String title;
  final VoidCallback? onTap;
  final Widget? badge;

  const ProfileMenuItem(
      {Key? key,
      required this.iconUrl,
      required this.title,
      this.onTap,
      this.badge})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: badge != null
            ? BoxDecoration(
                color: redColor.withOpacity(0.08),
                borderRadius: BorderRadius.circular(10),
              )
            : null,
        margin: const EdgeInsets.only(bottom: 5),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Image.asset(
            iconUrl,
            width: 24,
          ),
          const SizedBox(width: 18),
          Expanded(
            child: Text(
              textAlign: TextAlign.start,
              title,
              style: blackTextStyle.copyWith(
                fontWeight: medium,
              ),
            ),
          ),
          badge ?? const SizedBox(width: 70),
        ]),
      ),
    );
  }
}
