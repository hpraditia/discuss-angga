import 'package:flutter/material.dart';
import 'package:moyee/shared/theme.dart';

class UserAvatar extends StatelessWidget {
  final double width;
  final double height;
  final Widget? icon;

  const UserAvatar(
      {Key? key, required this.width, required this.height, this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 120,
        height: 120,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: AssetImage('assets/img_profile.png'),
          ),
        ),
        // ignore: unnecessary_null_in_if_null_operators
        child: icon ?? null);
  }
}

class IconUserAvatar extends StatelessWidget {
  final Widget? icon;
  const IconUserAvatar({Key? key, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topRight,
      child: Container(
        width: 35,
        height: 35,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: whiteColor,
        ),
        child: Center(
          // ignore: unnecessary_null_in_if_null_operators
          child: icon ?? null,
        ),
      ),
    );
  }
}
