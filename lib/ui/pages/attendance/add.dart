import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:geolocator/geolocator.dart';
// import 'package:geocoding/geocoding.dart';
import 'package:lottie/lottie.dart';
import 'package:mapbox_api/mapbox_api.dart';

import 'package:moyee/blocs/attendance/attendance_bloc.dart';
import 'package:moyee/blocs/auth/auth_bloc.dart';
import 'package:moyee/models/attendance/add_form.dart';
import 'package:moyee/models/attendance/parameter.dart';
import 'package:moyee/models/attendance/today.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/attendance/maps.dart';
import 'package:moyee/ui/widgets/buttons.dart';

import 'package:moyee/ui/widgets/forms.dart';

MapboxApi mapbox = MapboxApi(
    accessToken:
        'pk.eyJ1IjoiaGFuYWZpcSIsImEiOiJjbGJnNW9mMDQwY3o3M25xdTZrYncwand0In0.tnrDtjecahxRqkYKhIaLtA');

class AddAttendancePage extends StatefulWidget {
  const AddAttendancePage({Key? key}) : super(key: key);

  @override
  State<AddAttendancePage> createState() => _AddAttendancePageState();
}

class _AddAttendancePageState extends State<AddAttendancePage> {
  String location = 'Sedang mencari lokasi anda';
  String nameLocation = '';
  String cityName = 'Memperbarui lokasi anda';
  double latitude = 0;
  double longitude = 0;
  UserModel? user;
  TodayAttendanceModel? todayAttendance;

  @override
  void initState() {
    _determinePosition();
    super.initState();

    final authState = context.read<AuthBloc>().state;
    if (authState is AuthSuccess) {
      user = authState.data;
    }

    final attendanceState = context.read<AttendanceBloc>().state;
    if (attendanceState is AttendanceTodaySuccess) {
      todayAttendance = attendanceState.data;
    }
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);

    final Map<String, dynamic> office = {
      'latitude': -6.136582356263779,
      'longitude': 106.81255466032925,
    };

    final distanceFromOffice = Geolocator.distanceBetween(position.latitude,
        position.longitude, office['latitude'], office['longitude']);

    //dummy hotel
    // final distanceFromOffice = Geolocator.distanceBetween(-6.136582356263779,
    // 106.81255466032925, -6.137093046700769, 106.81301788028503);

    if (distanceFromOffice <= 100) {
      setState(() {
        longitude = position.longitude;
        latitude = position.latitude;

        location =
            'Jl. Pintu Besar Utara No.3, RW.6, Pinangsia, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11110';
        nameLocation = 'Museum Bank Indonesia';
        cityName = 'Jakarta, Indonesia';
      });
    } else {
      // final response = await placemarkFromCoordinates(
      //     position.latitude, position.longitude,
      //     localeIdentifier: "id_ID");

      //using mapbox for genereate address
      final response = await mapbox.reverseGeocoding.request(
        coordinate: <double>[
          position.latitude,
          position.longitude,
        ],
      );

      // if (response.isNotEmpty) {
      if (response.error == null) {
        setState(() {
          longitude = position.longitude;
          latitude = position.latitude;

          final alamat = response.features?[0].properties?.address ?? '';

          location = alamat;
          nameLocation = alamat;
          cityName = alamat;
          location = '$alamat ${response.features?[1].placeName}';
          nameLocation = '${response.features?[0].text}';
          cityName = '';
          final splitted = location.split(',');
          int loop = splitted.length;

          for (var element in splitted) {
            if (loop == 2) {
              cityName += '$element ,';
            }
            if (loop == 1) {
              cityName += ' $element';
            }
            loop -= 1;
          }
        });
      }
    }

    return position;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text(
          'Buat Kehadiran',
          // style: whiteTextStyle.copyWith(),
        ),
        // iconTheme: IconThemeData(color: whiteColor),
        // backgroundColor: primaryColor,
      ),
      // ignore: prefer_const_constructors
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              color: whiteColor,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            buildDate(),
            Positioned(
              top: 130,
              child: Container(
                decoration: BoxDecoration(
                  color: whiteColor,
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(20)),
                ),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height - 250,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 22, horizontal: 22),
                  child: buildForm(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDate() {
    return Positioned(
      top: 0,
      left: 0,
      right: 0,
      child: BlocProvider(
        create: (context) =>
            AttendanceBloc()..add(AttendanceGetDataToday(user!)),
        child: BlocBuilder<AttendanceBloc, AttendanceState>(
          builder: (context, state) {
            if (state is AttendanceTodaySuccess) {
              todayAttendance = state.data;
              return Container(
                color: lightBackgroundColor,
                width: MediaQuery.of(context).size.width,
                height: 150,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: blackColor,
                    borderRadius:
                        const BorderRadius.vertical(top: Radius.circular(24)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(cityName,
                            style: whiteTextStyle.copyWith(
                              fontWeight: medium,
                            )),
                        Text(
                          state.data.attendanceTime,
                          style: whiteTextStyle.copyWith(
                            fontSize: 38,
                            fontWeight: bold,
                          ),
                        ),
                        Text(state.data.attendanceText,
                            style: whiteTextStyle.copyWith(
                              fontSize: 14,
                            )),
                      ],
                    ),
                  ),
                ),
              );
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }

  Widget buildForm() {
    final descriptionController = TextEditingController(text: '');
    final typeAttendanceController = TextEditingController(text: '');

    return BlocConsumer<AttendanceBloc, AttendanceState>(
      listener: (context, state) {
        if (state is AddAttendanceSuccess) {
          AttendanceBloc().add(AttendanceRefreshDashboardData(
              ParameterAttendanceFormModel(
                  employeeId: user?.employeeId, limit: "4", orderBy: "DESC")));
          Navigator.pop(context);
        }
      },
      builder: (context, state) {
        return Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 120,
                  height: 180,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    clipBehavior: Clip.antiAlias,
                    child: (latitude != 0 && longitude != 0)
                        ? CustomMaps(
                            latitude: latitude,
                            longitude: longitude,
                          )
                        : Container(
                            color: lightBackgroundColor,
                            child: Lottie.asset(
                                'assets/animations/loading-map.json')),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        nameLocation,
                        style: blackTextStyle.copyWith(
                          fontSize: 16,
                          fontWeight: semiBold,
                        ),
                      ),
                      const SizedBox(height: 20),
                      Text(
                        location,
                        style: blackTextStyle.copyWith(
                          fontSize: 12,
                          fontWeight: medium,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(height: 30),
            todayAttendance?.attendanceStatus == 'Clock In'
                ? CustomDropdown(
                    title: 'Tipe Pekerjaan',
                    items: const [
                      DropdownMenuItem(
                        value: 'wfo',
                        child: Text('WFO'),
                      ),
                      DropdownMenuItem(
                        value: 'wfh',
                        child: Text('WFH'),
                      ),
                      DropdownMenuItem(
                        value: 'dinas',
                        child: Text('Perjalanan Dinas'),
                      ),
                      DropdownMenuItem(
                        value: 'sakit',
                        child: Text('Sakit'),
                      ),
                      DropdownMenuItem(
                        value: 'ijin',
                        child: Text('Ijin'),
                      ),
                    ],
                    onChanged: (value) {
                      typeAttendanceController.text = value;
                    })
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Tipe pekerjaan",
                        style: blackTextStyle.copyWith(
                          fontWeight: medium,
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        todayAttendance?.attendanceStatus ?? "Attendance",
                        style: blackTextStyle.copyWith(
                          fontWeight: bold,
                        ),
                      ),
                    ],
                  ),
            const SizedBox(height: 30),
            CustomFormField(
              title: 'Keterangan Pekerjaan',
              maxLines: 3,
              controller: descriptionController,
            ),
            const SizedBox(height: 30),
            CustomFilledButton(
              title: 'Simpan ${todayAttendance?.attendanceStatus}',
              width: MediaQuery.of(context).size.width,
              onPressed: () {
                if (todayAttendance != null) {
                  // print("type attendance: " + );
                  context.read<AttendanceBloc>().add(AddAttendance(
                        AddAttendanceFormModel(
                          address: '$nameLocation $location',
                          longlat:
                              '${longitude.toString()},${latitude.toString()}',
                          time: todayAttendance!.attendanceFullDateTime,
                          date: todayAttendance!.attendanceDate,
                          type: typeAttendanceController.text,
                          description: descriptionController.text,
                          status: todayAttendance!.attendanceStatus,
                          employeeId: user!.employeeId,
                          employeeCode: user!.username,
                        ),
                      ));
                }
              },
            )
          ],
        );
      },
    );
  }
}
