import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:iconify_flutter/icons/eva.dart';

import 'package:date_picker_timeline/date_picker_timeline.dart';

import 'package:moyee/blocs/attendance/attendance_bloc.dart';
import 'package:moyee/blocs/auth/auth_bloc.dart';

import 'package:moyee/models/user.dart';
import 'package:moyee/shared/helpers.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/attendance/item_attendance.dart';

// MapboxApi mapbox = MapboxApi(
//     accessToken:
//         'pk.eyJ1IjoiaGFuYWZpcSIsImEiOiJjbGJnNW9mMDQwY3o3M25xdTZrYncwand0In0.tnrDtjecahxRqkYKhIaLtA');

class AppsAttendance extends StatefulWidget {
  const AppsAttendance({Key? key}) : super(key: key);

  @override
  State<AppsAttendance> createState() => _AppsAttendanceState();
}

class _AppsAttendanceState extends State<AppsAttendance> {
  UserModel? user;
  DateTime _selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();

    final authState = context.read<AuthBloc>().state;
    if (authState is AuthSuccess) {
      user = authState.data;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      appBar: AppBar(
        title: const Text(
          'Kehadiran',
          // style: whiteTextStyle.copyWith(),
        ),
        // iconTheme: IconThemeData(color: whiteColor),
        // backgroundColor: primaryColor,
      ),
      // ignore: prefer_const_constructors
      body: BlocProvider(
        create: (context) => AttendanceBloc()..add(AttendanceGetData(user!)),
        child: SizedBox(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Stack(
            children: [
              Container(
                color: lightBackgroundColor,
                width: double.infinity,
                height: double.infinity,
              ),
              buildFilter(),
              Positioned(
                top: 180,
                child: Container(
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius:
                        const BorderRadius.vertical(top: Radius.circular(20)),
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height - 250,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 22, horizontal: 22),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Detail",
                            style: blackTextStyle.copyWith(
                              fontSize: 16,
                              fontWeight: bold,
                            )),
                        const SizedBox(
                          height: 30,
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            child: BlocBuilder<AttendanceBloc, AttendanceState>(
                              builder: (context, state) {
                                if (state is GetAttendanceSuccess) {
                                  return Column(
                                    children: state.data.map((attendance) {
                                      return AttendanceItem(
                                        time: attendance.shortTime,
                                        date: attendance.date,
                                        id: attendance.id,
                                        type: attendance.type,
                                        employeeId: attendance.employeeId,
                                        employeeCode: attendance.employeeCode,
                                        coordinate: attendance.coordinate,
                                        clockType: attendance.clockType,
                                        notes: attendance.notes,
                                        address: attendance.address,
                                        formatTime: attendance.formatTime,
                                        employeeName: attendance.employeeName,
                                      );
                                    }).toList(),
                                  );
                                }

                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    // ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildFilter() {
    return Positioned(
      top: 0,
      left: 0,
      right: 0,
      child: BlocProvider(
        create: (context) =>
            AttendanceBloc()..add(AttendanceGetDataToday(user!)),
        child: BlocBuilder<AttendanceBloc, AttendanceState>(
          builder: (context, state) {
            if (state is AttendanceTodaySuccess) {
              return Container(
                color: lightBackgroundColor,
                width: MediaQuery.of(context).size.width,
                height: 230,
                child: Container(
                  height: double.infinity,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: lightBackgroundColor,
                    borderRadius:
                        const BorderRadius.vertical(top: Radius.circular(24)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 30),
                    child: Column(
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text("Hari ini"),
                                  Text(
                                    dateDMY(_selectedDate),
                                    style: blackTextStyle.copyWith(
                                      fontWeight: bold,
                                      fontSize: 16,
                                    ),
                                  )
                                ],
                              ),
                              GestureDetector(
                                onTap: () async {
                                  await showDateRangePicker(
                                      context: context,
                                      builder: (context, child) {
                                        return Theme(
                                          data: Theme.of(context).copyWith(
                                            appBarTheme: AppBarTheme(
                                              color: primaryColor,
                                              titleTextStyle: whiteTextStyle,
                                              iconTheme: IconThemeData(
                                                color: whiteColor,
                                              ),
                                            ),
                                            datePickerTheme:
                                                DatePickerThemeData(
                                              headerHelpStyle:
                                                  whiteTextStyle.copyWith(
                                                fontSize: 14,
                                              ),
                                              rangePickerHeaderHelpStyle:
                                                  whiteTextStyle.copyWith(
                                                fontSize: 14,
                                              ),
                                              headerForegroundColor: whiteColor,
                                            ),
                                            iconTheme: IconThemeData(
                                              color: whiteColor,
                                            ),
                                            colorScheme: ColorScheme.light(
                                              primary: primaryColor,
                                              onPrimary: whiteColor,
                                              onSurface: primaryColor,
                                            ),
                                          ),
                                          child: child!,
                                        );
                                      },
                                      initialDateRange: DateTimeRange(
                                          start: DateTime(
                                              DateTime.now().year,
                                              DateTime.now().month,
                                              DateTime.now().day - 3),
                                          end: DateTime(
                                              DateTime.now().year,
                                              DateTime.now().month,
                                              DateTime.now().day + 3)),
                                      firstDate: DateTime(2015),
                                      lastDate: DateTime(2050),
                                      helpText: "Silahkan pilih tanggal",
                                      cancelText: "Batal",
                                      confirmText: "Selesai",
                                      initialEntryMode:
                                          DatePickerEntryMode.calendar);
                                },
                                child: const Row(
                                  children: [
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(5.0),
                                      child: Iconify(Eva.calendar_outline,
                                          size: 30),
                                    ),
                                  ],
                                ),
                              )
                            ]),
                        const SizedBox(
                          height: 30,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: greyColor,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: DatePicker(
                              DateTime(DateTime.now().year,
                                  DateTime.now().month, DateTime.now().day - 3),
                              width: 60,
                              height: 85,
                              initialSelectedDate: DateTime.now(),
                              selectionColor: blackColor,
                              selectedTextColor: whiteColor,
                              daysCount: 7,
                              locale: "id_ID",
                              onDateChange: (date) {
                                // New date selected
                                setState(() {
                                  _selectedDate = date;
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }

            return const Text("Loading");
          },
        ),
      ),
    );
  }
}
