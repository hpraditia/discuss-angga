import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moyee/blocs/auth/auth_bloc.dart';
import 'package:moyee/models/auth/login_form.dart';
import 'package:moyee/shared/helpers.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/buttons.dart';
import 'package:moyee/ui/widgets/forms.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final usernameController = TextEditingController(text: '');
  final passwordController = TextEditingController(text: '');

  bool validate() {
    if (usernameController.text.isEmpty || passwordController.text.isEmpty) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocConsumer<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthSuccess) {
          Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
        }

        if (state is AuthFailed) {
          showCustomSnackbar(context, state.e);
        }
      },
      builder: (context, state) {
        return ListView(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          children: [
            Container(
                width: 100,
                height: 100,
                margin: const EdgeInsets.only(
                  top: 100,
                  bottom: 100,
                ),
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/img_logo_light.png')))),
            Text(
              'Masuk Aplikasi',
              style:
                  blackTextStyle.copyWith(fontSize: 20, fontWeight: semiBold),
            ),
            Text("Silahkan isi formulir dibawah sesuai akun anda",
                style: greyTextStyle.copyWith(fontWeight: medium)),
            const SizedBox(height: 20),
            Container(
              padding: const EdgeInsets.all(22),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: whiteColor),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // USERNAME
                  // NOTE: EMAIL INPUT
                  CustomFormField(
                    title: 'Nama pengguna',
                    controller: usernameController,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  // NOTE: PASSWORD INPUT
                  CustomFormField(
                    title: 'Kata sandi',
                    obscureText: true,
                    controller: passwordController,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  const SizedBox(height: 8),
                  Align(
                      alignment: Alignment.centerRight,
                      child: Text('Lupa kata sandi ?', style: blueTextStyle)),
                  const SizedBox(height: 30),
                  CustomFilledButton(
                    title: 'Masuk',
                    onPressed: () {
                      if (validate()) {
                        context.read<AuthBloc>().add(
                              AuthLogin(
                                LoginModel(
                                  username: usernameController.text,
                                  password: passwordController.text,
                                ),
                              ),
                            );
                      } else {
                        showCustomSnackbar(context,
                            'Silahkan isi nama pengguna dan kata sandi yang sesuai');
                      }
                    },
                  ),
                ],
              ),
            )
          ],
        );
      },
    ));
  }
}
