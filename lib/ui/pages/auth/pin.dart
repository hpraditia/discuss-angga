import 'package:flutter/material.dart';
import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:iconify_flutter/icons/heroicons.dart';
// import 'package:heroicons/heroicons.dart';
import 'package:moyee/shared/helpers.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/pin/button.dart';

class PinPage extends StatefulWidget {
  const PinPage({Key? key}) : super(key: key);

  @override
  State<PinPage> createState() => _PinPageState();
}

class _PinPageState extends State<PinPage> {
  final TextEditingController pinController = TextEditingController(text: '');
  String pin = '111111';

  addPin(String number) {
    if (pinController.text.length < 6) {
      setState(() {
        pinController.text = pinController.text + number;
      });
    }
    if (pinController.text.length == 6) {
      print(pinController.text);
      print(pin);
      if (pinController.text == pin) {
        Navigator.pop(context, true);
      } else {
        showCustomSnackbar(context, 'PIN yang anda masukkan salah');
      }
    }
  }

  deletePin() {
    if (pinController.text.isNotEmpty) {
      setState(() {
        pinController.text =
            pinController.text.substring(0, pinController.text.length - 1);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: darkBackgroundColor,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 57),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Kode PIN',
                  style: whiteTextStyle.copyWith(
                    fontSize: 20,
                    fontWeight: semiBold,
                  )),
              const SizedBox(height: 60),
              SizedBox(
                width: 200,
                child: TextFormField(
                  controller: pinController,
                  obscureText: true,
                  obscuringCharacter: '*',
                  enabled: false,
                  style: whiteTextStyle.copyWith(
                    fontWeight: medium,
                    fontSize: 36,
                    letterSpacing: 16,
                  ),
                  decoration: InputDecoration(
                    disabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: greyColor),
                    ),
                  ),
                  cursorColor: greyColor,
                ),
              ),
              const SizedBox(
                height: 60,
              ),
              Wrap(
                spacing: 40,
                runSpacing: 40,
                children: [
                  PinButton(
                      text: '1',
                      onTap: () {
                        addPin('1');
                      }),
                  PinButton(
                      text: '2',
                      onTap: () {
                        addPin('2');
                      }),
                  PinButton(
                      text: '3',
                      onTap: () {
                        addPin('3');
                      }),
                  PinButton(
                      text: '4',
                      onTap: () {
                        addPin('4');
                      }),
                  PinButton(
                      text: '5',
                      onTap: () {
                        addPin('5');
                      }),
                  PinButton(
                      text: '6',
                      onTap: () {
                        addPin('6');
                      }),
                  PinButton(
                      text: '7',
                      onTap: () {
                        addPin('7');
                      }),
                  PinButton(
                      text: '8',
                      onTap: () {
                        addPin('8');
                      }),
                  PinButton(
                      text: '9',
                      onTap: () {
                        addPin('9');
                      }),
                  const SizedBox(
                    height: 60,
                    width: 60,
                  ),
                  PinButton(text: '0', onTap: () {}),
                  GestureDetector(
                    onTap: () {
                      deletePin();
                    },
                    child: Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: numberBackgroundColor,
                      ),
                      child: Center(
                        child: Iconify(
                          Heroicons.backspace_solid,
                          color: whiteColor,
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
