import 'package:flutter/material.dart';
import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:iconify_flutter/icons/heroicons.dart';

import 'package:moyee/blocs/auth/auth_bloc.dart';
import 'package:moyee/blocs/employee/employee_bloc.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/buttons.dart';
import 'package:moyee/ui/widgets/cards.dart';
import 'package:moyee/ui/widgets/forms.dart';
import 'package:moyee/ui/widgets/profile/avatar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final idController = TextEditingController(text: '');
  final nameController = TextEditingController(text: '');
  final emailController = TextEditingController(text: '');
  final genderController = TextEditingController(text: '');
  final placeBirthController = TextEditingController(text: '');
  final dateBirthController = TextEditingController(text: '');
  final addressController = TextEditingController(text: '');
  UserModel? user;

  @override
  void initState() {
    super.initState();

    final authState = context.read<AuthBloc>().state;

    if (authState is AuthSuccess) {
      user = authState.data;
      nameController.text = authState.data.name!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ubah Profil'),
      ),
      body: BlocProvider(
        create: (context) => EmployeeBloc()..add(GetEmployeeData(user!)),
        child: BlocConsumer<EmployeeBloc, EmployeeState>(
          listener: (context, state) {},
          builder: (context, state) {
            if (state is EmployeeSuccess) {
              idController.text = state.data.id!;
              nameController.text = state.data.name!;
              emailController.text = state.data.email!;
              genderController.text = state.data.gender!;
              placeBirthController.text = state.data.placeOfBirth!;
              dateBirthController.text = state.data.dateOfBirth!;
              addressController.text = state.data.address!;

              return ListView(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                children: [
                  const SizedBox(height: 30),
                  Center(
                    child: RoundedCard(
                      color: whiteColor,
                      width: double.infinity,
                      cardChild: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Center(
                              child: UserAvatar(
                                width: 120,
                                height: 120,
                                icon: IconUserAvatar(
                                    icon: Container(
                                        padding: const EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: greenColor,
                                        ),
                                        child: Iconify(
                                          Heroicons.camera_solid,
                                          color: whiteColor,
                                          size: 18,
                                        ))),
                              ),
                            ),
                            const SizedBox(height: 30),
                            CustomFormField(
                              title: 'ID Pengguna',
                              controller: idController,
                            ),
                            const SizedBox(height: 16),
                            CustomFormField(
                              title: 'Nama Lengkap',
                              controller: nameController,
                            ),
                            const SizedBox(height: 16),
                            CustomFormField(
                              title: 'Alamat Email',
                              controller: emailController,
                            ),
                            const SizedBox(height: 16),
                            CustomFormField(
                              title: 'Jenis Kelamin',
                              isEnabled: false,
                              controller: genderController,
                            ),
                            const SizedBox(height: 16),
                            CustomFormField(
                              title: 'Tempat Lahir',
                              controller: placeBirthController,
                            ),
                            const SizedBox(height: 16),
                            CustomFormDatepicker(
                              title: 'Tanggal Lahir',
                              controller: dateBirthController,
                            ),
                            const SizedBox(height: 16),
                            CustomFormField(
                              title: 'Alamat',
                              maxLines: 3,
                              controller: addressController,
                            ),
                            const SizedBox(height: 30),
                            CustomFilledButton(
                              title: 'Simpan Perubahan',
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, '/success-edit-profile');
                              },
                            ),
                          ]),
                    ),
                  )
                ],
              );
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
