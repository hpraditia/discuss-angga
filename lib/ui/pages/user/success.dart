import 'package:lottie/lottie.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/buttons.dart';
import 'package:flutter/material.dart';

class SuccessPage extends StatelessWidget {
  const SuccessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Lottie.network(
            //   'https://assets7.lottiefiles.com/private_files/lf30_nsqfzxxx.json',
            //   reverse: false,
            //   repeat: false,
            // ),
            Text(
              'Berhasil Menyimpan!',
              style: blackTextStyle.copyWith(
                fontSize: 20,
                fontWeight: semiBold,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 26,
            ),
            Text(
              'Data kamu berhasil diperbarui \n dan tersimpan dengan aman disistem kami.',
              style: greyTextStyle.copyWith(
                fontSize: 16,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 50,
            ),
            CustomFilledButton(
              width: 183,
              title: 'Kembali',
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, '/home', (route) => false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
