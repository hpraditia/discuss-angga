import 'package:flutter/material.dart';

import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/buttons.dart';
import 'package:moyee/ui/widgets/cards.dart';
import 'package:moyee/ui/widgets/forms.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  State<ChangePasswordPage> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  // String _dropdownValue = 'Laki laki';

  // void dropdownCallback(String? selectedValue) {
  //   if (selectedValue is String) {
  //     setState(() {
  //       _dropdownValue = selectedValue;
  //     });

  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Atur Kata sandi'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        children: [
          const SizedBox(height: 30),
          Center(
            child: RoundedCard(
              color: whiteColor,
              width: double.infinity,
              cardChild: const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomFormField(
                      title: 'Kata sandi lama',
                    ),
                    SizedBox(height: 16),
                    CustomFormField(
                      title: 'Kata sandi baru',
                    ),
                    SizedBox(height: 30),
                    CustomFilledButton(
                      title: 'Simpan Perubahan',
                    ),
                  ]),
            ),
          )
        ],
      ),
    );
  }
}
