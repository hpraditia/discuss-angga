import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconify_flutter/iconify_flutter.dart';
import 'package:iconify_flutter/icons/heroicons.dart';

import 'package:moyee/blocs/auth/auth_bloc.dart';
import 'package:moyee/services/auth/auth.dart';
import 'package:moyee/shared/theme.dart';

import 'package:moyee/ui/widgets/profile/menu_item.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'My Profile',
          ),
        ),
        body: BlocConsumer<AuthBloc, AuthState>(
          listener: (context, state) {
            // TODO: implement listener
          },
          builder: (context, state) {
            if (state is AuthLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            if (state is AuthSuccess) {
              return ListView(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24,
                ),
                children: [
                  const SizedBox(
                    height: 40,
                  ),
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 30, vertical: 22),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: whiteColor,
                    ),
                    child: Column(children: [
                      Container(
                          width: 120,
                          height: 120,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage('assets/img_profile.png'),
                            ),
                          ),
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              width: 28,
                              height: 28,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: whiteColor,
                              ),
                              child: Center(
                                child: Iconify(
                                  Heroicons.check_circle_solid,
                                  size: 24,
                                  //style: HeroIconStyle.solid,
                                  color: greenColor,
                                ),
                              ),
                            ),
                          )),
                      const SizedBox(height: 16),
                      Text(
                        state.data.name!,
                        style: blackTextStyle.copyWith(
                          fontSize: 18,
                          fontWeight: medium,
                        ),
                      ),
                      const SizedBox(height: 40),
                      ProfileMenuItem(
                        iconUrl: 'assets/ic_edit_profile.png',
                        title: 'Ubah Profile',
                        onTap: () async {
                          // if (await Navigator.pushNamed(context, '/pin') == true) {
                          //   Timer(const Duration(milliseconds: 500), () {
                          Navigator.pushNamed(context, '/edit-profile');
                          //   });
                          // }
                        },
                        badge: Iconify(
                          Heroicons.exclaimation_circle_solid,
                          size: 30,
                          //style: HeroIconStyle.solid,
                          color: redColor,
                        ),
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/ic_wallet.png',
                        title: 'Data Kartu',
                        onTap: () {
                          Navigator.pushNamed(context, '/pin');
                        },
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/ic_pin.png',
                        title: 'Atur Kata Sandi',
                        onTap: () async {
                          if (await Navigator.pushNamed(context, '/pin') ==
                              true) {
                            Timer(const Duration(milliseconds: 500), () {
                              Navigator.pushNamed(context, '/change-password');
                            });
                          }
                        },
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/ic_reward.png',
                        title: 'My Rewards',
                        onTap: () {
                          Navigator.pushNamed(context, '/pin');
                        },
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/ic_help.png',
                        title: 'Help Center',
                        onTap: () {
                          Navigator.pushNamed(context, '/pin');
                        },
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/ic_report.png',
                        title: 'Feedback',
                        onTap: () {
                          Navigator.pushNamed(context, '/pin');
                        },
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/ic_logout.png',
                        title: 'Logout',
                        onTap: () {
                          AuthService().clearLocalStorage();
                        },
                      ),
                    ]),
                  ),
                  const SizedBox(height: 87),
                ],
              );
            }

            return Container();
          },
        ));
  }
}
