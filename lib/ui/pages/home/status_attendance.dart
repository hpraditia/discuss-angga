import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moyee/blocs/attendance/attendance_bloc.dart';
import 'package:moyee/blocs/auth/auth_bloc.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/helpers.dart';
import 'package:moyee/shared/theme.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class StatusAttendance extends StatefulWidget {
  const StatusAttendance({Key? key}) : super(key: key);

  @override
  State<StatusAttendance> createState() => _StatusAttendanceState();
}

class _StatusAttendanceState extends State<StatusAttendance> {
  UserModel? user;

  @override
  void initState() {
    final authState = context.read<AuthBloc>().state;

    if (authState is AuthSuccess) {
      user = authState.data;
    }
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AttendanceBloc()..add(AttendanceGetDataToday(user!)),
      child: BlocBuilder<AttendanceBloc, AttendanceState>(
        builder: (context, state) {
          if (state is AttendanceTodaySuccess) {
            return AnimatedSwitcher(
              duration: const Duration(seconds: 1),
              transitionBuilder: (child, animation) => ScaleTransition(
                scale: animation,
                child: child,
              ),
              child: state.data.attendanceStatus == 'Clock In'
                  ? iddle()
                  : working(),
            );
          }

          if (state is AttendanceFailed) {
            print('failed => ' + state.e);
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget working() {
    return Container(
      key: const Key('working'),
      margin: const EdgeInsets.only(top: 20),
      height: 130,
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: blackColor,
      ),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Semangat',
                  style: whiteTextStyle.copyWith(
                    fontSize: 14,
                    fontWeight: bold,
                  )),
              Text('Kamu sudah bekerja selama',
                  style: whiteTextStyle.copyWith(
                    fontSize: 12,
                    fontWeight: medium,
                  )),
              const SizedBox(
                height: 5,
              ),
              Text(
                '8 Jam',
                style: whiteTextStyle.copyWith(
                  fontSize: 20,
                  fontWeight: bold,
                ),
              ),
            ],
          ),
          const Spacer(),
          SleekCircularSlider(
            appearance: CircularSliderAppearance(
                animDurationMultiplier: 2.5,
                spinnerDuration: 3000,
                infoProperties: InfoProperties(
                  mainLabelStyle: whiteTextStyle.copyWith(
                    fontSize: 12,
                    fontWeight: semiBold,
                  ),
                ),
                size: 100,
                customColors: CustomSliderColors(
                  dynamicGradient: true,
                  trackColor: greyColor,
                  dotColor: greenColor,
                  progressBarColors: [
                    const Color(0xFF27187e),
                    primaryColor,
                  ],
                ),
                customWidths: CustomSliderWidths(progressBarWidth: 10)),
            min: 0,
            max: 100,
            initialValue: 8 / 9 * 100,
          ),
        ],
      ),
    );
  }

  Widget iddle() {
    return Container(
      key: const Key('iddle'),
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: redColor,
      ),
      width: MediaQuery.of(context).size.width - 48,
      height: 130,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Penting!',
              style: whiteTextStyle.copyWith(
                fontSize: 20,
                fontWeight: bold,
              )),
          const SizedBox(height: 10),
          Text(
              'Saat ini kamu belum memulai pekerjaan, segera buat kehadiran anda!',
              style: whiteTextStyle.copyWith(
                fontSize: 14,
                fontWeight: medium,
              )),
          const SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }
}
