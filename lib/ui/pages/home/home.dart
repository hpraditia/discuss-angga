import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:iconify_flutter/iconify_flutter.dart';

import 'package:iconify_flutter/icons/eva.dart';
import 'package:iconify_flutter/icons/heroicons.dart';
import 'package:iconify_flutter/icons/ri.dart';

import 'package:moyee/blocs/attendance/attendance_bloc.dart';
import 'package:moyee/blocs/auth/auth_bloc.dart';
import 'package:moyee/blocs/employee/employee_bloc.dart';
import 'package:moyee/models/attendance/parameter.dart';
import 'package:moyee/models/user.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/pages/home/features_section.dart';
import 'package:moyee/ui/pages/home/geotagging_section.dart';
import 'package:moyee/ui/pages/home/status_attendance.dart';
import 'package:moyee/ui/widgets/buttons.dart';
import 'package:moyee/ui/widgets/home/service_item.dart';
import 'package:moyee/ui/widgets/home/tips_item.dart';
import 'package:moyee/ui/widgets/home/attendance_item.dart';
import 'package:moyee/ui/widgets/home/user_item.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserModel? user;

  @override
  void initState() {
    super.initState();
    final authState = context.read<AuthBloc>().state;
    if (authState is AuthSuccess) {
      user = authState.data;
    }

    final attendanceState = context.read<AttendanceBloc>().state;
    if (attendanceState is AddAttendanceSuccess) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: buildNavigationBar(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/add-attendance');
        },
        backgroundColor: primaryColor,
        child: Iconify(
          Heroicons.finger_print_solid,
          color: whiteColor,
          size: 30,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: RefreshIndicator(
        color: primaryColor,
        onRefresh: () async {
          context.read<EmployeeBloc>().add(GetEmployeeTeamMember(user!));
          context.read<AttendanceBloc>().add(AttendanceRefreshDashboardData(
              ParameterAttendanceFormModel(
                  employeeId: user?.employeeId, limit: "2", orderBy: "DESC")));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: SingleChildScrollView(
            child: Column(
              children: [
                buildProfile(context),
                const StatusAttendance(
                  key: Key('statusAttendance'),
                ),
                // AddGeotagging(),
                // const FeaturesApplication(),
                buildServices(),
                buildLatestAttendance(context),
                buildYourTeam(context),
                const SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildNavigationBar(BuildContext context) {
    return BottomAppBar(
      color: whiteColor,
      shape: const CircularNotchedRectangle(),
      clipBehavior: Clip.antiAlias,
      notchMargin: 6,
      elevation: 0,
      child: Theme(
        data: Theme.of(context).copyWith(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          elevation: 0,
          backgroundColor: whiteColor,
          selectedItemColor: primaryColor,
          unselectedItemColor: blackColor,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          selectedLabelStyle: primaryTextStyle.copyWith(
            fontSize: 10,
            fontWeight: medium,
          ),
          unselectedLabelStyle: blackTextStyle.copyWith(
            fontSize: 10,
            fontWeight: medium,
          ),
          items: [
            BottomNavigationBarItem(
              label: 'Beranda',
              icon: const Iconify(Eva.home_outline),
              activeIcon: Iconify(
                Eva.home_outline,
                color: primaryColor,
              ),
            ),
            BottomNavigationBarItem(
              icon: const Iconify(Eva.pie_chart_outline),
              activeIcon: Iconify(
                Eva.pie_chart_fill,
                color: primaryColor,
              ),
              label: 'Laporan',
            ),
            const BottomNavigationBarItem(
              icon: Iconify(
                Heroicons.finger_print,
                size: 0,
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: const Iconify(Eva.inbox_outline),
              activeIcon: Iconify(
                Eva.inbox_fill,
                color: primaryColor,
              ),
              label: 'Notifikasi',
            ),
            BottomNavigationBarItem(
              icon: const Iconify(Ri.newspaper_line),
              activeIcon: Iconify(
                Ri.newspaper_fill,
                color: primaryColor,
              ),
              label: 'Berita',
            )
          ],
        ),
      ),
    );
  }

  Widget buildProfile(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 60,
      ),
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state is AuthSuccess) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Halo,',
                      style: greyTextStyle.copyWith(
                        fontSize: 16,
                      ),
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      state.data.name!,
                      style: blackTextStyle.copyWith(
                        fontSize: 20,
                        fontWeight: semiBold,
                      ),
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, '/profile');
                  },
                  child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: state.data.profilePicture != null
                              ? NetworkImage(state.data.profilePicture!,
                                  scale: 1) as ImageProvider
                              : const AssetImage('assets/img_profile.png'),
                        ),
                      ),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          width: 16,
                          height: 16,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: whiteColor,
                          ),
                          child: Center(
                            child: Iconify(
                              Heroicons.exclaimation_circle_solid,
                              size: 20,
                              color: redColor,
                            ),
                          ),
                        ),
                      )),
                ),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget buildServices() {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Layanan Aplikasi',
            style: blackTextStyle.copyWith(
              fontSize: 16,
              fontWeight: semiBold,
            ),
          ),
          const SizedBox(
            height: 14,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ServiceItem(
                iconUrl: 'assets/features/calendar.png',
                title: 'Kehadiran',
                onTap: () {
                  print('Halo, ini pesan yang dicetak di konsol!');
                  Navigator.pushNamed(context, '/attendance-apps');
                },
              ),
              ServiceItem(
                iconUrl: 'assets/features/overtime.png',
                title: 'Lembur',
                onTap: () {},
              ),
              ServiceItem(
                iconUrl: 'assets/features/payslips.png',
                title: 'Slip Gaji',
                onTap: () {},
              ),
              ServiceItem(
                iconUrl: 'assets/features/more.png',
                title: 'Lainya',
                onTap: () {},
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildLatestAttendance(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 30,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Riwayat Kehadiran',
            style: blackTextStyle.copyWith(
              fontSize: 16,
              fontWeight: semiBold,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 14),
            padding: const EdgeInsets.all(22),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: whiteColor,
            ),
            child: SingleChildScrollView(
              child: BlocProvider(
                create: (context) => AttendanceBloc()
                  ..add(AttendanceGetDashboardData(ParameterAttendanceFormModel(
                      employeeId: user?.employeeId,
                      limit: "4",
                      orderBy: "DESC"))),
                child: BlocBuilder<AttendanceBloc, AttendanceState>(
                  builder: (context, state) {
                    if (state is AttendanceLoading) {
                      return Center(
                        child: CircularProgressIndicator(
                          color: primaryColor,
                        ),
                      );
                    }
                    if (state is GetAttendanceSuccess) {
                      return Column(
                        children: state.data.map((attendance) {
                          return GestureDetector(
                            onTap: () {
                              print(attendance.type);
                            },
                            child: AttendanceItem(
                                category: attendance.type,
                                title: attendance.clockType,
                                time:
                                    '${attendance.formatTime}, ${attendance.shortTime}',
                                value: '-'),
                          );
                        }).toList(),
                      );
                    }

                    return Center(
                      child: CircularProgressIndicator(
                        color: primaryColor,
                      ),
                    );
                  },
                ),
              ),
              //   const CustomTextButton(
              //     title: 'Selengkapnya',
              //   )
              // ],
              // ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildYourTeam(Bui) {
    return BlocProvider(
      create: (context) => EmployeeBloc()..add(GetEmployeeTeamMember(user!)),
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Tim Kamu',
                style: blackTextStyle.copyWith(
                  fontSize: 16,
                  fontWeight: semiBold,
                ),
              ),
              CustomTextButton(
                  title: 'Lihat Semua',
                  width: 100,
                  textStyle: greyTextStyle.copyWith(
                    fontSize: 14,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/team-member');
                  }),
            ],
          ),
          const SizedBox(height: 14),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: BlocBuilder<EmployeeBloc, EmployeeState>(
              builder: (context, state) {
                if (state is EmployeeTeamMemberSuccess) {
                  return Row(
                    children: state.data.map((member) {
                      return GestureDetector(
                        onTap: () {
                          print(member.name);
                        },
                        child: UserItem(
                          imageUrl: member.profilePicture,
                          name: member.name,
                          position: member.position,
                        ),
                      );
                    }).toList(),
                  );
                }

                return const Row(
                  children: [
                    UserItemLoading(),
                    UserItemLoading(),
                    UserItemLoading(),
                    UserItemLoading(),
                  ],
                );
              },
            ),
          )
        ]),
      ),
    );
  }
}
