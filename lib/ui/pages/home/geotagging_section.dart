import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
// import 'package:heroicons/heroicons.dart';
import 'package:lottie/lottie.dart';

import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/attendance/maps.dart';

class AddGeotagging extends StatefulWidget {
  const AddGeotagging({Key? key}) : super(key: key);

  @override
  State<AddGeotagging> createState() => _AddGeotaggingState();
}

class _AddGeotaggingState extends State<AddGeotagging> {
  String location = 'Sedang mencari lokasi anda';
  String nameLocation = '';
  String cityName = 'Memperbarui lokasi anda';
  double latitude = 0;
  double longitude = 0;

  Future<Position> getUserLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);

    final Map<String, dynamic> office = {
      'latitude': -6.136582356263779,
      'longitude': 106.81255466032925,
    };

    final distanceFromOffice = Geolocator.distanceBetween(position.latitude,
        position.longitude, office['latitude'], office['longitude']);
    //using mapbox for genereate address
    if (distanceFromOffice <= 100) {
      setState(() {
        longitude = position.longitude;
        latitude = position.latitude;

        location =
            'Jl. Pintu Besar Utara No.3, RW.6, Pinangsia, Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11110';
        nameLocation = 'Museum Bank Indonesia';
        cityName = 'Jakarta, Indonesia';
      });
    } else {
      //using mapbox for genereate address
      final response =
          await placemarkFromCoordinates(position.latitude, position.longitude);

      // mapbox.reverseGeocoding.request(
      //   coordinate: <double>[
      //     position.latitude,
      //     position.longitude,
      //   ],
      // );

      if (response.isNotEmpty) {
        setState(() {
          longitude = position.longitude;
          latitude = position.latitude;

          var alamat = response[0].toString();
          location = alamat;
          nameLocation = alamat;
          cityName = alamat;
          // location = '$alamat ${response.features?[1].placeName}';
          // nameLocation = '${response.features?[0].text}';
          // cityName = '';
          // final splitted = location.split(',');
          // int loop = splitted.length;

          // for (var element in splitted) {
          //   if (loop == 2) {
          //     cityName += '$element ,';
          //   }
          //   if (loop == 1) {
          //     cityName += ' $element';
          //   }
          //   loop -= 1;
          // }
        });
      }
    }

    return position;
  }

  @override
  void initState() {
    getUserLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: [
              Positioned(
                top: 10,
                right: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width - 228,
                  height: 180,
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: (latitude != 0 && longitude != 0)
                        ? CustomMaps(
                            latitude: latitude,
                            longitude: longitude,
                          )
                        : Container(
                            color: whiteColor,
                            child: Lottie.asset(
                                'assets/animations/loading-map.json')),
                  ),
                ),
              ),
              Container(
                width: 200,
                height: 200,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        nameLocation,
                        style: whiteTextStyle.copyWith(
                          fontSize: 14,
                          fontWeight: medium,
                        ),
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        location,
                        style: whiteTextStyle.copyWith(
                            fontSize: 12,
                            fontWeight: medium,
                            overflow: TextOverflow.ellipsis),
                        maxLines: 3,
                      ),
                      const Spacer(),
                      TextButton(
                        onPressed: () {},
                        style: TextButton.styleFrom(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 10,
                            ),
                            backgroundColor: whiteColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(56))),
                        child: Text('Tambah Geotagging',
                            style: primaryTextStyle.copyWith(
                                fontSize: 12, fontWeight: semiBold)),
                      ),
                    ]),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
