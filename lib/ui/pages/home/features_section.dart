import 'package:flutter/material.dart';
import 'package:moyee/shared/theme.dart';
import 'package:moyee/ui/widgets/home/service_item.dart';

class FeaturesApplication extends StatelessWidget {
  const FeaturesApplication({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('asdas');
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Layanan Aplikasi',
            style: blackTextStyle.copyWith(
              fontSize: 16,
              fontWeight: semiBold,
            ),
          ),
          const SizedBox(
            height: 14,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ServiceItem(
                iconUrl: 'assets/features/calendar.png',
                title: 'Kehadiran',
                onTap: () {},
              ),
              ServiceItem(
                iconUrl: 'assets/features/overtime.png',
                title: 'Lembur',
                onTap: () {},
              ),
              ServiceItem(
                iconUrl: 'assets/features/flight.png',
                title: 'Cuti',
                onTap: () {},
              ),
              ServiceItem(
                iconUrl: 'assets/features/more.png',
                title: 'Lainya',
                onTap: () {},
              ),
            ],
          )
        ],
      ),
    );
  }
}
